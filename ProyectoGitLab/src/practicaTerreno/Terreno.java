package practicaTerreno;

public class Terreno {
    //Atributos de la clase
    private int numTerreno;
    private float ancho;
    private float largo;
    
    //Metodos
    //Metodos constructores
    //Omisicion
    
    public Terreno(){
    //Hace referencia a la clase y a los datos
        this.numTerreno=0;
        this.ancho=0.0f;
        this.largo=0.0f;
    }

    //Constructor por argumentos
    public Terreno(int numTerreno, float ancho, float largo) {
        this.numTerreno = numTerreno;
        this.ancho = ancho;
        this.largo = largo;
    }

    //Copia
    public Terreno(Terreno otro) {
    this.numTerreno=otro.numTerreno;
    this.ancho=otro.ancho;
    this.largo=otro.largo;
    }
    
    //Metodos Set y Get
    public int getNumTerreno() {
        return numTerreno;
    }

    public void setNumTerreno(int numTerreno) {
        this.numTerreno = numTerreno;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }
    
    //Metodos de comportamiento
    public float calcularPerimetro(){
    float perimetro=0.0f;
    perimetro=(this.ancho+this.largo)*2;
    return perimetro;
    }
    
    public float calcularArea(){
    float area=0.0f;
    area=this.ancho+this.largo;
    return area;

    }
    
  }
