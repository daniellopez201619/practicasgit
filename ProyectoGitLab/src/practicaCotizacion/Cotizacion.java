package practicaCotizacion;

public class Cotizacion {
//Atributos de la clase
    private int numCotizacion;
    private String descripcion;
    private float precio;
    private float porcentajeInicial;
    private int plazo;
    
    public Cotizacion(){
    this.numCotizacion=0;
    this.descripcion="";
    this.precio=0.0f;
    this.porcentajeInicial=0.0f;
    this.plazo=0;
    }
    
    //Constructor por argumentos
    public Cotizacion(int numCotizacion, String descripcion, float precio, float porcentajeInicial, int plazo, float pagoInicial, int total) {
        this.numCotizacion = numCotizacion;
        this.descripcion = descripcion;
        this.precio = precio;
        this.porcentajeInicial = porcentajeInicial;
        this.plazo = plazo;
    }
    
    //Copia
    public Cotizacion(Cotizacion otro) {
        this.numCotizacion = otro.numCotizacion;
        this.descripcion = otro.descripcion;
        this.precio = otro.precio;
        this.porcentajeInicial = otro.porcentajeInicial;
        this.plazo = otro.plazo;
    }
    
    //Metodos Set y Get
    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajeInicial() {
        return porcentajeInicial;
    }

    public void setPorcentajeInicial(float porcentajeInicial) {
        this.porcentajeInicial = porcentajeInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    //Metodos de comportamiento
    public float calcularPagoInicial(){
    float pagoInicial=0;
    pagoInicial = this.precio*this.porcentajeInicial;
    return pagoInicial;
    }
    
     public float calcularTotal(){
    float total=0;
    total = this.precio-this.calcularPagoInicial();
    return total;
    }
    
     public float calcularPagoMensual(){
    float pagoMensual=0;
    pagoMensual = this.calcularTotal()/this.plazo;
    return pagoMensual;
    }
}