package practicaCotizacion;

public class Practica02 {
    
    public static void main(String[] args) {
        //Generar objeto construido por omisicion
        Cotizacion cotizacion = new Cotizacion();
        cotizacion.setPrecio(220000.0f);
        cotizacion.setPorcentajeInicial(0.25f);
        cotizacion.setPlazo(36);
        
        System.out.println("Pago inicial= "+cotizacion.calcularPagoInicial());
        System.out.println("Pago inicial= "+cotizacion.calcularPagoInicial());
        System.out.println("Total= "+cotizacion.calcularTotal());
        System.out.println("Pago mensual= "+cotizacion.calcularPagoMensual());
    }
    
}