package practicaProductos;

public class Practica03 {

    public static void main(String[] args) {
        // Generar objeto construido por omisicion
        
        Productos productos = new Productos();
        
        productos.setCodigo("147");
        productos.setDescripcion("Atun");
        productos.setUnidadMedida("Piezas");
        productos.setPrecioCompra(10.00f);
        productos.setPrecioVenta(15.00f);
        productos.setCantidad(100);
        
        System.out.println("Precio de Venta= "+productos.calcularPrecioVenta());
        System.out.println("Precio de compra= "+productos.calcularPrecioCompra());
        System.out.println("Ganancia= "+productos.calcularGanancia());
    }
    
}